package com.example.task1;

public class PaymentProcessor {

    public void processPaymentEvent(Event event) {
        if (event.getType() == "SEPA") {
            processSepaEvent(event);
            return;
        }
        if (event.getType() == "SOFORT") {
            processSofortEvent(event);
            return;
        }
        if (event.getType() == "ACH") {
            processAchEvent(event);
            return;
        }
        processDefaultEvent(event);
    }

    private void processSepaEvent(Event event) {

    }

    private void processSofortEvent(Event event) {

    }

    private void processAchEvent(Event event) {

    }

    private void processDefaultEvent(Event event) {

    }
}