package com.example.task1;

import lombok.Data;

@Data
public class Event {
    private String type;
    private String payload;
}
